import React from 'react';
import ReactDOM from 'react-dom';
import { UnlinkedIcon } from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<UnlinkedIcon />, div);
  ReactDOM.unmountComponentAtNode(div);
});
