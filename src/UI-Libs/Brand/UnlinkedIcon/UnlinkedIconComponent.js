import React, { Component } from 'react';

import { Icon } from '..';

export class UnlinkedIcon extends Component {
    render() {
        return (
            <img src={ Icon } alt="" />
        )
    }
}

export default UnlinkedIcon;