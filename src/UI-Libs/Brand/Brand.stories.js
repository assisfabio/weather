/// TODO: See https://github.com/storybooks/addon-jsx
import React from 'react';

import { storiesOf } from '@storybook/react';

import { LinkedIcon, UnlinkedIcon, LinkedLogo } from './';

storiesOf('Logo', module)
  .add('Logo w/ Link', () => <LinkedLogo />)
  .add('Icon w/ Link', () => <LinkedIcon />)
  .add('Icon w/out Link', () => <UnlinkedIcon />);