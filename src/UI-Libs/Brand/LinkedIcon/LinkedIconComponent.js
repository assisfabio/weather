import React, { Component } from 'react';

import { Icon } from '..';

export class LinkedIcon extends Component {
    render() {
        return (
            <a>
                <img src={ Icon } alt="" />
            </a>
        )
    }
}

export default LinkedIcon;