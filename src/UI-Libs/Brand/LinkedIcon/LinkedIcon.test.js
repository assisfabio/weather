import React from 'react';
import ReactDOM from 'react-dom';
import { LinkedIcon } from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LinkedIcon />, div);
  ReactDOM.unmountComponentAtNode(div);
});
