import React from 'react';
import ReactDOM from 'react-dom';
import { LinkedLogo } from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LinkedLogo />, div);
  ReactDOM.unmountComponentAtNode(div);
});
