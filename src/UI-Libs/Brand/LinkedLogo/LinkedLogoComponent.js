import React, { Component } from 'react';
import style from 'styled-components';

import { Logo } from '../';

const LinkedLogoSt = style.a`
    display: inline-block;
    & img {
        width: 180px;
    }
`;
export class LinkedLogo extends Component {
    render() {
        return (
            <LinkedLogoSt>
                <img src={ Logo } alt="" />
            </LinkedLogoSt>
        )
    }
}

export default LinkedLogo;