import React from 'react';
import styled from 'styled-components';
import { Theme } from '../Styles';

const Styles = {};
Styles.CardSt = styled.div`
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
    color: ${Theme.Colors.greyDarker};
    margin: 10px;
    max-width: 300px;
    min-width: 200px;
    position: relative;
    overflow: hidden;
    text-align: center;
    transition: box-shadow .3s ease-in-out;
    width: 90%;
    &:hover {
        box-shadow: 5px 8px 20px rgba(0,0,0,0.2);
        z-index: 2;
        ${Styles.FooterSt} {
            .humidity,
            .pressure {
                padding: 5px 10px;
                max-height: 80px;
            }
        }
    }
`;
Styles.HeaderSt = styled.div`
    border-bottom: 1px solid ${Theme.Colors.greyLight};
    padding: 5px 10px;
    h2 {
        font-size: 1em;
        font-weight: 400;
        line-height: 1.5em;
        margin: 0;
    }
`;
Styles.BodySt = styled.div`
    padding: 10px;
    p {
        color: ${props => {
            let ret;
            if (props.temp <= props.unitMetric(5)) {
                ret = Theme.Colors.info;
            }
            else if (props.temp > props.unitMetric(5) && props.temp <= props.unitMetric(22)) {
                ret = Theme.Colors.warning;
            }
            else {
                ret = Theme.Colors.danger;
            }
            return ret;
        }};
        display: inline-block;
        font-size: 5.5em;
        margin: 0;
        position: relative;
        transition: color .3s ease-in-out;
        sup {
            font-size: .4em;
            position: absolute;
            right: -.8em;
        }
    }
`;
Styles.BodySt.defaultProps = { temp: 0, unitMetric: (i) => i};
Styles.FooterSt = styled.div`
    background-color: rgba(241, 241, 241 ,0.5);
    .humidity,
    .pressure {
        box-sizing: border-box;
        float: left;
        font-size: 1.2em;
        max-height: 0;
        padding: 0;
        overflow: hidden;
        text-align: center;
        transition: all .3s ease-in-out;
        width: 50%;
        label {
            color: ${Theme.Colors.greyDark};
            display: block;
            font-size: 11px;
            text-transform: uppercase;
        }
        span {
            font-size: .6em;
        }
    }
    .date {
        clear: both;
        color: ${Theme.Colors.greyDark};
        font-size: .7em
        font-style: italic;
        line-height: 1.5em;
        padding: 5px 10px;
    }
`;

const WeatherCardTemplate = (data) => {
    
    Styles.BodySt.defaultProps = {
        ...data.main,
        ...{
            unitMetric: (value) => { // Celsius to Kelvin
                if (data.unit.id === 'standart' || !data.unit.id) {
                    return value + 273.15;
                }
                else if (data.unit.id === 'imperial') { // Celsius to Fahrenheit
                    return (value * (9/5)) + 32;
                }
                return value;
            }
        }
    };

    const timeConverter = (UNIX_timestamp) => {
        const a = new Date(UNIX_timestamp * 1000);
        const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        const year = a.getFullYear();
        const month = months[a.getMonth()];
        const date = a.getDate();
        const hour = a.getHours();
        const min = a.getMinutes();
        const sec = a.getSeconds();
        let time = `
            Updated at 
            ${(new Date().toDateString() < a.toDateString()) ? `${date} ${month} ${year}` : ''}
             
            ${hour < 10 ? '0' + hour : hour }:${min < 10 ? '0' + min : min}:${sec < 10 ? '0' + sec : sec}
        `;
        return time;
    }

    return (<Styles.CardSt>
        <Styles.HeaderSt>
            <h2>{data.name}, {data.sys.country}</h2>
        </Styles.HeaderSt>
        <Styles.BodySt>
            <p>
                {parseInt(data.main.temp)}<sup>{data.unit.sign}</sup>
            </p>
        </Styles.BodySt>
        <Styles.FooterSt>
            <div className="humidity">
                <label>Humidity</label>
                {data.main.humidity}
                <span>%</span>
            </div>
            <div className="pressure">
                <label>Pressure</label>
                {data.main.pressure}
                <span>hPa</span>
            </div>
            <div className="date">{timeConverter(data.dt)}</div>
        </Styles.FooterSt>
    </Styles.CardSt>);
};

const WeatherCardGhostTemplate = () => {
    return (<Styles.CardSt className="ghosted">
        <Styles.HeaderSt>
            <h2 className="text-markup">&nbsp;</h2>
        </Styles.HeaderSt>
        <Styles.BodySt>
            <p className="text-markup">&nbsp;</p>
        </Styles.BodySt>
        <Styles.FooterSt>
            <div className="humidity">
                <label>Humidity</label>
                XX
                <span>%</span>
            </div>
            <div className="pressure">
                <label>Pressure</label>
                XXX
                <span>hPa</span>
            </div>
            <div className="date">Updated at XX:XX:XX</div>
        </Styles.FooterSt>
    </Styles.CardSt>);
};

export {
    WeatherCardTemplate,
    WeatherCardGhostTemplate
}