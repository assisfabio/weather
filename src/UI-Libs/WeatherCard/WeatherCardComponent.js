import { Component } from 'react';
import { WeatherCardTemplate, WeatherCardGhostTemplate } from './WeatherCardTemplate';

export class WeatherCard extends Component {
    constructor (props) {
        super(props);
        this.state = null;
    }

    handleUpdateData = () => {
        // TODO Mudar a URL para um modelo dinâmico
        // 1 - Receber a API Key de um .env; ou
        // 2 - Receber como props e montar atraves de um UrlBuilder fazendo replace das variáveis
        // PS.: Olhar Axios para substiruir FetchAPI, verificar o suporte dela a methodos dinâmicos e montar um RequestBuilder, se for possível
        fetch(`//api.openweathermap.org/data/2.5/weather?q=${this.props.city}&units=${this.props.unit.id}&APPID=b0ea82752199682d5419c3cb9477c0b6`)
        // TODO interceptar de forma centralizada as chamadas de API
        .then((response) => {
            if (response && response.status === 200){
                return response.text();
            }
            else {
                // TODO informar usuario sobre erro na chamada
                console.log("Mensagem de Erro");
                return null;
            }
        })
        .then((data) => {
            try {
                data = JSON.parse(data);
            } catch (error) {
                // TODO informar Logs de Aplicação sobre o Erro
                data = null;
            }
            this.setState(data);
        });
    }

    componentWillMount() {
        this.handleUpdateData();
    }

    componentWillUnmount(){
        console.log('SENDO DESMONTADO');
    }

    componentWillReceiveProps(){
        this.oldProps = this.props;
        this.readyToUpdate = true;
    }

    componentDidUpdate() {
        const oldCity = (this.oldProps && this.oldProps.city && this.oldProps.city.toLowerCase());
        const newCity = (this.props && this.props.city && this.props.city.toLowerCase());
        const oldUnit = (this.oldProps && this.oldProps.unit && this.oldProps.unit.id);
        const newUnit = (this.props && this.props.unit && this.props.unit.id);

        if (this.readyToUpdate && (oldCity && oldUnit) && (oldCity !==  newCity || oldUnit !== newUnit)) {
            this.handleUpdateData();
            this.readyToUpdate = false;
        }
    }

    render() {
        return (this.state) ? WeatherCardTemplate({...this.state, ...this.props}) : WeatherCardGhostTemplate();
    }
}

export default WeatherCard;