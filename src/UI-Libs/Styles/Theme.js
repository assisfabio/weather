import { createGlobalStyle } from 'styled-components'
const GlobalStyle = createGlobalStyle`
    html, body {
        margin: 0;
        padding: 0;
    }
    .text-markup {
        background: rgba(0,0,0,0.1);
        border-radius: .5em;
        display: inline-block;
        width: 80%;
    }
`;

const Theme = {
    Colors: {
        greyLightest: "rgba(241, 241, 241 ,0.5)",
        greyLighter: "#f1f1f1",
        greyLight : "#ebebeb",
        grey: "#333333",
        greyDark: "#b4b4b4",
        greyDarker: "#737c84",
        info: "#69a3ff",
        warning: "#ff9632",
        danger: "#ed1946",
        bg: "#fff"
    }
};
export default Theme;
export { GlobalStyle };