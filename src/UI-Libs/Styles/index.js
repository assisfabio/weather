import Theme, { GlobalStyle } from './Theme';

export {
    Theme,
    GlobalStyle
};