import { combineReducers } from 'redux';
import { ConfigReducer /*, CardListReducer*/ } from './Weather/';

const rootReducers = combineReducers({
    configs: ConfigReducer,
    // cardList: CardListReducer
});

export default rootReducers;