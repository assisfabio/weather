import register, { unregister } from './registerServiceWorker';
import reducers from './reducers';

it('Call register without crashing', () => {
    const process = {
        env: {
            NODE_ENV: 'production'
        }
    };
    const navigator = {
        serviceWorker: true
    }
    register();
});

it('Call unregister without crashing', () => {
    const navigator = {
        serviceWorker: true
    }
    unregister();
});