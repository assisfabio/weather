import { Component } from 'react';
import { connect } from 'react-redux';

import { ConfigTemplate } from './ConfigTemplate';
import ConfigActions from './ConfigActions';

export class Config extends Component {
    constructor() {
        super();
        this.state = {};
    }
    
    handleSubmit = (event) => {
        event.preventDefault()
        this.props.ConfigActions({
            unit: event.target.units.value,
            cities: this.state.cities,
            open: false
        });
    }

    handleAddCity = () => {
        if (!this.state.cities.find(city => city.toLocaleLowerCase() === this.newCity.value.toLocaleLowerCase())) {
            this.state.cities.push(this.newCity.value);
            this.newCity.value = '';
            this.setState({});
        }
        else {
            alert('Cidade já incluida');
        }
    }

    handleRemoveCity = (city) => {
        this.setState({
            cities: this.state.cities.filter((item) => item !== city)
        });
        this.props.ConfigActions({});
    }

    componentWillMount(){
        this.setState({ cities: this.props.configs.cities });
        this.props.ConfigActions(this.props.configs);
    }

    render() {
        // ConfigSt.defaultProps = this.props;
        return ConfigTemplate(this);
    }
}

const mapStateToProps = state =>({ ...state });
export default connect(mapStateToProps, { ConfigActions })(Config);