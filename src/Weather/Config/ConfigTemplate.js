import React from 'react';
import styled from 'styled-components';
import { Theme } from '../../UI-Libs/Styles';

const Styles = {};
Styles.ConfigSt = styled.div`
    position: absolute;
    right: .5em;
    top: 50%;
    transform: translate(0,-50%);
    .icon {
        background-color: ${Theme.Colors.bg};
        border: 0;
        border-radius: 50%;
        color: ${Theme.Colors.grayDark};
        font-size: 32px;
        height: 1em;
        line-height: .8em;
        outline: none;
        padding: 0;
        transform: rotate(${props => (props.configs.open) ? 0 : 90 }deg);
        transition: transform .3s ease-in-out;
        width: 1em;
    }
    .window {
        background-color: #fff;
        box-shadow: 0 0 2px rgba(0, 0, 0, 0.2);
        font-size: 13px;
        height: ${props => (props.configs.open) ? 195 : 0 }px;
        padding: ${props => (props.configs.open) ? '15px 10px' : '0' };
        position: absolute;
        text-align: left;
        top: ${props => (props.configs.open) ? '100' : '60' }%
        transition: all .3s ease-in-out;
        right: ${props => (props.configs.open) ? 0 : 10 }px;
        width: ${props => (props.configs.open) ? 305 : 0 }px;
        &:before {
            background-color: #fff;
            border: ${props => (props.configs.open) ? '5' : '0' }px solid #fff;
            box-shadow: -1px -1px 0.5px rgba(0, 0, 0, 0.07);
            content: "";
            position: absolute;
            right: 11px;
            top: -5px;
            transform: rotate(45deg);
        }
        .window-content {
            max-height: 100%;
            overflow: hidden;
            width: 100%;
            form {
                width: 285px;
            }
        }
        .tag {
            background-color: ${Theme.Colors.greyLighter};
            border-radius: 3px;
            box-shadow: 0 0 1px rgba(0,0,0,.8);
            color: ${Theme.Colors.grey};
            cursor: pointer;
            display: inline-block;
            font: normal 11px monospace;
            letter-spacing: -.06em;
            margin: 3px 4px 4px 2px;
            padding: 1px 13px 1px 4px;
            position: relative;
            transition: background-color .3s ease-in-out;
            &:after {
                content: '\00d7';
                font-size: 1.2em;
                line-height: .5em;
                position: absolute;
                right: 3px;
                transform: translate(0, -50%);
                top: 50%;
            }
            &:hover {
                background-color: ${Theme.Colors.greyDark};
            }
        }
        h4 {
            margin: 0;
        }
        .radioGroup {
            align-items: center;
            display: flex;
            justify-content: center;
            margin-bottom: 10px;
            label {
                cursor: pointer;
                overflow: hidden;
                position: relative;
                width: 100%;
                input {
                    height: 0;
                    position: absolute;
                    top: -100%;
                    visibility: hidden;
                    width: 0;
                }
                input + span {
                    background-color: ${Theme.Colors.greyLight}
                    border: 1px solid ${Theme.Colors.greyDark};
                    display: block;
                    font: normal 11px/1.3em monospace;
                    letter-spacing: -.06em;
                    text-align: center;
                    transition: all .3s ease-in-out;
                }
                input:checked + span {
                    background-color: ${Theme.Colors.info}
                    border-color: ${Theme.Colors.info};
                    color: ${Theme.Colors.bg};
                }
            }
        }
    }
`;

const ConfigTemplate = (ctrl) => {
    
    Styles.ConfigSt.defaultProps = ctrl.props;

    return (<Styles.ConfigSt>
        <button className="icon" onClick={() => {
            ctrl.props.ConfigActions({
                "open": !ctrl.props.configs.open
            });
        }}>⋯</button>

        <div className="window">
            <div className="window-content">
                <form onSubmit={ctrl.handleSubmit}>
                    <h4>Shows Temperature:</h4>
                    <div className="radioGroup">
                        {ctrl.props.configs.contents.units.map((unit, i) => {
                            return (<label key={i}>
                                <input type="radio" name="units" defaultChecked={ctrl.props.configs && ctrl.props.configs.unit === unit.id} value={unit.id} />
                                <span>{unit.value}({unit.sign})</span>
                            </label>)
                        })}
                    </div>

                    <h4>Shows Cities:</h4>
                    <div className="textGroup">
                        {
                            ctrl.state.cities.map((city, i) => {
                                return (<div className="tag" key={i} onClick={() => ctrl.handleRemoveCity(city)}>{city}</div>);
                            })
                        }
                        <input type="hidden" name="stringCities" defaultValue={ctrl.state.cities} />
                        <input type="text" ref={node => (ctrl.newCity = node)} />
                        <button type="button" onClick={ctrl.handleAddCity}>ADD</button>
                    </div>

                    <button type="submit">Salvar</button>
                </form>
            </div>
        </div>
    </Styles.ConfigSt>);
};

export {
    ConfigTemplate
}