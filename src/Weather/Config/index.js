import Config from './ConfigComponent';
import ConfigActions from './ConfigActions';
import ConfigReducer from './ConfigReducer'

export {
  Config,
  ConfigActions,
  ConfigReducer
};