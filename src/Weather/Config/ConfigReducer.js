let cities = (window.localStorage && window.localStorage.getItem('weather.cities')) || ['Nuuk,GL', 'Urubici,BR', 'Nairobi,KE'];
let unit = (window.localStorage && window.localStorage.getItem('weather.unit')) || 'metric';

if (typeof cities === 'string') cities = JSON.parse(cities);

if (window.localStorage) {
    window.localStorage.setItem('weather.cities', JSON.stringify(cities));
    window.localStorage.setItem('weather.unit', unit);
}

const INITIAL_STATE = {
    contents: {
        units: [
            { id: "metric", value: "Celsius", sign: "°C"},
            { id: "imperial", value: "Fahrenheit", sign: "°F"},
            { id: "standart", value: "Kelvin", sign: "°K"}
        ]
    },
    unit,
    cities,
    open: false
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_CONFIG':
            if (window.localStorage) {
                if (action.payload.unit) {
                    window.localStorage.setItem('weather.unit', action.payload.unit);
                }

                if (action.payload.cities) {
                    window.localStorage.setItem('weather.cities', JSON.stringify(action.payload.cities));
                }
            }
            Object.assign(state, action.payload);
            return { ...state };

        default:
            return state;
    }
}