import React from 'react';
import ReactDOM from 'react-dom';
import { Config } from './ConfigComponent';
import { actionDefault } from './ConfigActions'
import reducer from './ConfigReducer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Config />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders with props', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Config config={[{item:1}, {item:2}]} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Call action without crashing', () => {
  const mockedData = 'teste';
  expect(actionDefault(mockedData)).toEqual({
    type: 'GET_CONFIG',
    payload: mockedData
  })
});

it('Call Reducer´s default case', () => {
  const mockedData = {
    type: 'none',
    payload: {
      config: { 
        item: 'teste'
      }
    }
  };
  expect(reducer(undefined, mockedData)).toEqual({});
})

it('Call Reducer´s Config case', () => {
  const mockedData = {
    type: 'GET_CONFIG',
    payload: {
      config: { 
        item: 'teste'
      }
    }
  };
  
  expect(reducer(undefined, mockedData))
    .toEqual(mockedData.payload.config);
})

it('Call Reducer´s Offer case', () => {
  const mockedData = {
    type: 'GET_CONFIG',
    payload: { 
      item: 'teste'
    }
  };
  
  expect(reducer(undefined, mockedData))
    .toEqual(mockedData.payload);
})
