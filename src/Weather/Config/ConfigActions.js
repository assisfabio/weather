export default function ConfigActions(data) {
    return {
        type: 'GET_CONFIG',
        payload: data
    };
}