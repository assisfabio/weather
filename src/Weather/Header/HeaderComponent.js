import React, { Component } from 'react';
import styled from 'styled-components';

import { LinkedLogo } from '../../UI-Libs';

import { Config } from '../Config';

const Styles = {};
Styles.HeaderST = styled.header`
    align-items: center;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.2);
    display: flex;
    height: 50px;
    justify-content: center;
    position: relative;
    text-align: center;
    z-index: 2;
`;
export class Header extends Component {
    render() {
        return (
            <Styles.HeaderST>
                <LinkedLogo />
                <Config />
            </Styles.HeaderST>
        )
    }
}

export default Header;