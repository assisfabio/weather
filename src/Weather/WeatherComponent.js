import React, { Component } from 'react';

import { GlobalStyle } from '../UI-Libs/Styles';

import { Header } from './Header';
import { Footer } from './Footer';
import { CardList } from './CardList';

export class Weather extends Component {
  render() {
    return (
      <main>
        <GlobalStyle />
        <Header />
        <div className="main">
          <CardList></CardList>
          <Footer />
        </div>
      </main>
    );
  }
}

export default Weather;