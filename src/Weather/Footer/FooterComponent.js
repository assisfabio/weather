import React, { Component } from 'react';
import styled from 'styled-components';

import { UnlinkedIcon } from '../../UI-Libs';

const FooterSt = styled.footer`
    font-size: 13px;
    height: 35px;
    line-height: 35px;
    padding: 3px;
    text-align: center;
    img {
        display: inline-block;
        height: 19px;
        margin: 0 .5em .5em 0;
        vertical-align: middle;
    }
`;

export class Footer extends Component {
    render() {
        return (
            <FooterSt>
                <UnlinkedIcon /> Weather Now  - &copy; { new Date().getFullYear() }
            </FooterSt>
        )
    }
}

export default Footer;