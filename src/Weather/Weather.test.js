import React from 'react';
import ReactDOM from 'react-dom';
import { Checkout, CheckoutActions, CheckoutReducer } from '.';
import { getWeatherInfo } from './WeatherActions'
import reducer from './WeatherReducer';


it('renders without crashing', () => {
  // const div = document.createElement('div');
  // ReactDOM.render(<Checkout />, div);
  // ReactDOM.unmountComponentAtNode(div);
});

it('Call action without crashing', () => {
  const mockedData = 'teste';
  expect(getWeatherInfo(mockedData)).toEqual({
    type: 'GET_CONFIG',
    payload: mockedData
  })
});

it('Call Reducer´s default case', () => {
  const mockedData = {
    type: 'none',
    payload: {
      configs: { 
        item: 'teste'
      }
    }
  };
  expect(reducer(undefined, mockedData)).toEqual({});
})

it('Call Reducer´s Config case', () => {
  const mockedData = {
    type: 'GET_CONFIG',
    payload: {
      configs: { 
        item: 'teste'
      }
    }
  };
  
  expect(reducer(undefined, mockedData))
    .toEqual(mockedData.payload.configs);
})