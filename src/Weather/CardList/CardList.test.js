import React from 'react';
import ReactDOM from 'react-dom';
import { CardList } from './';
import action from './CardListActions'
import reducer from './CardListReducer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CardList />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Call action without crashing', () => {
  const mockedData = 'teste';
  expect(action(mockedData)).toEqual({
    type: 'GET_CONFIG',
    payload: mockedData
  })
});

it('Call Reducer´s default case', () => {
  const mockedData = {
    type: 'none',
    payload: {
      config: { 
        item: 'teste'
      }
    }
  };
  expect(reducer(undefined, mockedData)).toEqual({});
})

it('Call Reducer´s Config case', () => {
  const mockedData = {
    type: 'GET_CONFIG',
    payload: {
      config: { 
        item: 'teste'
      }
    }
  };
  
  expect(reducer(undefined, mockedData))
    .toEqual(mockedData.payload.config);
})

it('Call Reducer´s Offer case', () => {
  const mockedData = {
    type: 'GET_CONFIG',
    payload: { 
      item: 'teste'
    }
  };
  
  expect(reducer(undefined, mockedData))
    .toEqual(mockedData.payload);
})
