import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Theme } from '../../UI-Libs/Styles';
import { CardListActions } from './';

import { WeatherCard } from '../../UI-Libs';

const CardListSt = styled.div`
  background-color: ${Theme.Colors.greyLighter};
  display: grid;
  height: calc(100vh - 85px);
  grid-gap: 10px;
  grid-template-columns: repeat( auto-fit, minmax(300px, 1fr));
  overflow: auto;
  padding: 10px;
  .grid-item {
    align-items: center;
    display: flex;
    justify-content: center;
    min-height: 240px;
  }
`;


export class CardList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(){
    this.setState({
      ...this.state,
      updated: new Date().getTime()
    });
  }
  render() {
    return (
     <CardListSt>
         {
          this.props.configs.cities.map((name, index) => 
            <div key={index} className="grid-item">
              <WeatherCard 
                city={name} 
                unit={this.props.configs.contents.units.find((item) => item.id === this.props.configs.unit)} 
              />
            </div>)
         }
     </CardListSt>
    );
  }
}

const mapStateToProps = state =>({ ...state });
export default connect(mapStateToProps, { CardListActions })(CardList);